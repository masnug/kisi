<?php namespace Routemedia\Kisi;

use Illuminate\Database\Eloquent\Model;

/**
* ModelRepository
*/
abstract class ModelRepository
{
    /**
     * Berisi array kaluasa
     *
     * @var     array
     */
    protected $clauses;

    /**
     * Digunakan untuk nama colom yg disamarkan, misal
     * $column_alias = array(
     *  'name' => 'full_name',
     *  'approved' => 'approved_at'
     * );
     *
     * @var     array
     */
    protected $column_alias;

    /**
     * Mendifinisikan daftar arg query yg akan diolah, contoh
     * $sortable = array('name');
     *
     * @var     array
     */
    protected $sortable;

    /**
     * Mendifinisikan daftar arg query yg akan diolah, contoh:
     * $filterable = array(
     *  'name' => 'contains',
     *  'approved' => array('at', 'after', 'before')
     * );
     *
     * @var     array
     */
    protected $filterable;

    /**
     * operator untuk filter
     *
     * @var     array
     */
    protected $filter_operators = array(
        'contains', 'prefix', 'suffix',
        'equal', 'min', 'max',
        'at', 'after', 'before',
        'is', 'in',
        'query'
    );

    /**
     * The model being queried
     *
     * @var     Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @param   Illuminate\Database\Eloquent\Model  $model
     * @param   array   $clauses
     * @return  void
     */
    public function __construct(Model $model, array $clauses = array())
    {
        $this->model = $model;
        $this->clauses = $clauses;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setClauses(array $clauses)
    {
        $this->clauses = $clauses;

        return $this;
    }

    public function getClauses()
    {
        return $this->clauses;
    }

    public function hasClause($element)
    {
        return array_key_exists($element, $this->clauses);
    }

    public function getClauseValue($element)
    {
        return $this->clauses[$element];
    }

    public function setFilterString($string = '')
    {
        $string = urldecode($string);
        if (preg_match_all('~\[([^\]\=]+)\=([^\]\=]+)\]~', $string, $matches, PREG_SET_ORDER)) {
            $clauses = array();
            foreach ($matches as $value) {
                $clauses[$value[1]] = $value[2];
            }

            $this->setClauses($clauses);
        }
    }

    public function getFilterString()
    {
        $string = '';
        foreach ($this->clauses as $key => $value) {
            $string .= "[{$key}={$value}]";
        }

        return $string;
    }

    public function applyOrders(array $clauses = array())
    {
        $clauses = $clauses ?: $this->getClauses();

        if (empty($this->sortable) || empty($clauses)) return $this;

        foreach ($this->sortable as $alias) {
            $column = isset($this->column_alias[$alias]) ? $this->column_alias[$alias] : $alias;
            $element_name = $alias . '-order';

            if (
                ! array_key_exists($element_name, $clauses) ||
                empty($clauses[$element_name])
            ) continue;

            $direction = $clauses[$element_name];

            $this->model = $this->model->orderBy($column, $direction);
        }

        return $this;
    }

    public function applyFilters(array $clauses = array())
    {
        $clauses = $clauses ?: $this->getClauses();

        if (empty($this->filterable) || empty($clauses)) return $this;

        foreach ($this->filterable as $alias => $operators) {
            $column = isset($this->column_alias[$alias]) ? $this->column_alias[$alias] : $alias;
            $operators = (array)$operators;

            foreach ($operators as $operator) {
                if (! in_array($operator, $this->filter_operators)) {
                    throw new \Exception('Operator "'.$operator.'" tidak diizinkan.', 1);
                }

                $element_name = $alias . '-' . $operator;
                if (
                    ! array_key_exists($element_name, $clauses) ||
                    empty($clauses[$element_name]) ||
                    (
                        ! is_array($clauses[$element_name]) &&
                        trim($clauses[$element_name]) == ''
                    )
                ) {
                    continue;
                } else {
                    $value = $clauses[$element_name];
                    $method_name = 'apply' . \Str::studly($element_name) . 'Filter';

                    if (method_exists($this, $method_name)) {
                        $this->model = $this->$method_name($this->model, $value);
                    }
                    else {
                        $filter_attr = compact('column', 'operator', 'value');
                        $this->model = $this->applyFilter($filter_attr);
                    }
                }
            }
        }

        return $this;
    }

    protected function applyFilter($attrs)
    {
        $column = $attrs['column'];
        $value = $attrs['value'];

        switch ($attrs['operator']) {
            case 'contains':
                $this->model = $this->model->where(function($query) use ($column, $value) {
                    $i = 0;
                    foreach (explode(' ', $value) as $_value) {
                        if ($i == 0) {
                            $query->where($column, 'like', "%{$_value}%");
                        }
                        else {
                            $query->orWhere($column, 'like', "%{$_value}%");
                        }
                        $i++;
                    }
                });
                break;

            case 'prefix':
                $this->model = $this->model->where($column, 'like', "{$value}%");
                break;

            case 'suffix':
                $this->model = $this->model->where($column, 'like', "%{$value}");
                break;

            case 'equal':
            case 'at':
            case 'is':
                $this->model = $this->model->where($column, '=', $value);
                break;

            case 'min':
            case 'after':
                $this->model = $this->model->where($column, '>=', $value);
                break;

            case 'max':
            case 'before':
                $this->model = $this->model->where($column, '<=', $value);
                break;

            case 'in':
                $this->model = $this->model->whereIn($column, $value);
                break;
        }

        return $this->model;
    }

    /**
     * Apply filter and order
     * @param   mixed   $clauses
     * @return  \Routemedia\Kisi\ModelRepository
     */
    public function applyFiltersAndOrders($clauses = null)
    {
        if ($clauses) {
            $this->setClauses($clauses);
        }

        return $this->applyFilters()->applyOrders();
    }

    /**
     * Alias for method applyFiltersAndOrders
     * @param   mixed   $clauses
     * @return  \Routemedia\Kisi\ModelRepository
     */
    public function applyOrdersAndFilters($clauses = null)
    {
        return $this->applyFiltersAndOrders($clauses);
    }

    /**
     * Execute the query from builder and get the first result.
     *
     * @param  array   $columns
     * @return array
     */
    public function getFirst($columns = array('*'))
    {
        return $this->model->take(1)->get($columns)->first();
    }

    /**
     * Execute the query from builder and get the top N result.
     *
     * @param  integer $take
     * @param  array   $columns
     * @return array
     */
    public function getTop($take = 25, $columns = array('*'))
    {
        return $this->model->take($take)->get($columns);
    }

    public function getAll($columns = array('*'))
    {
        return $this->model->get($columns);
    }

    public function getPaginate($per_page = 25, $columns = array('*'))
    {
        return $this->model->paginate($per_page, $columns);
    }
}
